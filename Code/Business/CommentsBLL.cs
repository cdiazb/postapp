﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class CommentsBLL
    {
        private Comment _comments = new Comment();

        /// <summary>
        /// Method to call the model function to get all comments by Post
        /// </summary>
        /// <param name="idPost"> int value Post Id</param>
        /// <returns>A list of Comment Objects that were written for this posts</returns>
        public List<Comment> GetCommentsByPost(int idPost)
        {
            try
            {
                return _comments.GetCommentsByPost(idPost);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to create a new comment
        /// </summary>
        /// <param name="comment"> string value comment text</param>
        /// <param name="idPost"> int value Post Id</param>
        /// <returns>Return a bool value if it was possible create or not</returns>
        public bool CreateComment(string comment, int idPost)
        {
            try
            {
                return new Comment()
                {
                    Comment1 = comment,
                    FkPost = idPost
                }.SaveComment();
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        // <summary>
        /// Method to delete all Comment  by Post
        /// </summary>
        /// <param name="idPost"> int value Post Id</param>
        public void DeleteCommentByPost(int idPost)
        {
            try
            {
                List<Comment> comments = _comments.GetCommentsByPost(idPost);
                foreach (Comment comment in comments)
                {
                    comment.DeleteComment();
                }
            }
            catch (Exception exp)
            {

            }
        }
    }
}
