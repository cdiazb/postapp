﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class PostBLL
    {
        private Post _post = new Post();

        /// <summary>
        /// Method to call the model function to get all posts from the DataBase
        /// </summary>
        /// <returns>A list of GeneralPost Objects with editor name, writer name and status name</returns>
        public List<GeneralPost> GetAllPosts()
        {
            try
            {
                return _post.GetPosts();
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to call the model function to get all posts that are in status 3 from the DataBase
        /// </summary>
        /// <returns>A list of GeneralPost Objects that are approved, with editor name, writer name and status name</returns>
        public List<GeneralPost> GetApprovedPosts()
        {
            try
            {
                return _post.GetPostByStatus(3);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to call the model function to get all posts that have been written by a writer
        /// </summary>
        /// <param name="idWriter"> int value Id writer</param>
        /// <returns>A list of GeneralPost Objects that were written by this writer, with editor name, writer name and status name</returns>
        public List<GeneralPost> GetPostsByWriter(int idWriter)
        {
            try
            {
                return _post.GetPostByWriter(idWriter);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to call the model function to get one post by id
        /// </summary>
        /// <param name="id"> int value Id Post</param>
        /// <returns>A GeneralPost Objects, with editor name, writer name and status name</returns>
        public GeneralPost GetPostsById(int id)
        {
            try
            {
                return _post.GetPostById(id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to call the model function to get all posts that have been processed by an editor
        /// </summary>
        /// <param name="idUser"> int value Id Editor</param>
        /// <returns>A list of GeneralPost Objects that have been processed by this Editor, with editor name, writer name and status name</returns>
        public List<GeneralPost> GetPostsByIdEditor(int idUser)
        {
            try
            {
                return _post.GetPostByEditor(idUser);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to call the model function to get all posts that are pending to be approved
        /// </summary>
        /// <returns>A list of GeneralPost Objects that are pending to be approved, with editor name, writer name and status name</returns>
        public List<GeneralPost> GetPendingPosts()
        {
            try
            {
                return _post.GetPostByStatus(2);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to create a new post
        /// </summary>
        /// <param name="title"> string value Post title</param>
        /// <param name="text"> string value Post Text</param>
        /// <param name="idWriter"> int value Id writer</param>
        public void CreatePost(string title, string text, int idWriter)
        {
            try
            {
                new Post()
                {
                    Title = title,
                    Text = text,
                    FkWriter = idWriter,
                    CreationDate = DateTime.Now,
                    FkStatus = 1
                }.SavePost();
            }
            catch (Exception exp)
            {

            }
        }

        /// <summary>
        /// Method to update an old post
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <param name="title"> string value Post title</param>
        /// <param name="text"> string value Post Text</param>
        public void EditPost(int id, string title, string text)
        {
            try
            {
                Post post = _post.GetSimplePostById(id);
                post.Title = title;
                post.Text = text;
                post.SavePost();
            }
            catch (Exception exp)
            {

            }
        }

        /// <summary>
        /// Method to submit a post 
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <param name="idUser"> int value Writer Id</param>
        /// <returns>Return a bool value if it was possible submit or not</returns>
        public bool SubmitPost(int id, int idUser)
        {
            try
            {
                Post post = _post.GetSimplePostById(id);
                if (post.FkWriter == idUser)
                {
                    return post.SubmitPost();
                }
                return false;
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        // <summary>
        /// Method to Approve a post 
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <param name="idUser"> int value Editor Id</param>
        /// <returns>Return a bool value if it was possible Approve or not</returns>
        public bool ApprovePost(int id, int idUser)
        {
            try
            {
                User usr = new User().GetUserById(idUser);
                Post post = _post.GetSimplePostById(id);
                if (post.FkStatus == 2 && usr.FkRol == 2)
                {
                    post.FkEditor = idUser;
                    return post.PublishPost();
                }
                return false;
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        // <summary>
        /// Method to Reject a post 
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <param name="idUser"> int value Editor Id</param>
        /// <returns>Return a bool value if it was possible Reject or not</returns>
        public bool RejectPost(int id, int idUser)
        {
            try
            {
                User usr = new User().GetUserById(idUser);
                Post post = _post.GetSimplePostById(id);
                if (post.FkStatus == 2 && usr.FkRol == 2)
                {
                    post.FkEditor = idUser;
                    return post.RejectPost();
                }
                return false;
            }
            catch (Exception exp)
            {
                return false;
            }
        }


        // <summary>
        /// Method to delete a post 
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <returns>Return a bool value if it was possible delete or not</returns>
        public bool DeletePost(int id)
        {
            try
            {
                CommentsBLL _comment = new CommentsBLL();
                Post post = _post.GetSimplePostById(id);
                _comment.DeleteCommentByPost(id);
                return post.DeletePost();
            }
            catch (Exception exp)
            {
                return false;
            }
        }
    }
}
