﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class UsersBLL
    {
        private User _user = new User();

        /// <summary>
        /// Method to send username and encripted password to verified if that users exists
        /// </summary>
        /// <param name="UserName"> UserName</param>
        /// <param name="password"> No encrypted password</param>
        /// <returns>An User object if the user exist or a null if it doesn't</returns>
        public User ValidateUser(string UserName, string password)
        {
            try
            {
                password = EncryptStringValue(password);
                return _user.GetUserByUserNameAndPassword(UserName, password);
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to encrypt a string
        /// </summary>
        /// <param name="StringValue"> string value </param>
        /// <returns>Returns the string encripted</returns>
        public string EncryptStringValue(string StringValue)
        {
            SHA1CryptoServiceProvider Provider = new SHA1CryptoServiceProvider();
            byte[] vectoBytes = Encoding.UTF8.GetBytes(StringValue);
            byte[] inArray = Provider.ComputeHash(vectoBytes);
            Provider.Clear();
            return Convert.ToBase64String(inArray);
        }

    }
}
