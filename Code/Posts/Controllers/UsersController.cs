﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Business;
using Model;

namespace Posts.Controllers
{
    public class UsersController : Controller
    {
        private UsersBLL _user = new UsersBLL();

        /// <summary>
        /// POST method to validate the username and password and store identity data
        /// </summary>
        /// <param name="UserName"> string value userName</param>
        /// <param name="Password"> String value no encrypted password</param>
        /// <returns>Redirect to home if is false or to post according the user rol</returns>
        [HttpPost]
        [Route("ValidateUser")]
        public ActionResult ValidateUser(string UserName, string Password)
        {
            try
            {
                if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
                {
                    return RedirectToAction("index", "home");
                }

                User ValidUser = _user.ValidateUser(UserName, Password);

                if (ValidUser != null)
                {
                    var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.PrimarySid, ValidUser.Id.ToString()),
                            new Claim(ClaimTypes.Name, ValidUser.Name),
                            new Claim(ClaimTypes.Role, ValidUser.FkRol.ToString()),
                        },
                        "ApplicationCookie");

                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;

                    authManager.SignIn(identity);
                    if(ValidUser.FkRol == 1)
                        return RedirectToAction("index", "posts");
                    else
                        return RedirectToAction("Approved", "posts");
                    
                }
                return RedirectToAction("index", "home");
            }
            catch (Exception exp)
            {
                return RedirectToAction("index", "home");
            }
        }

        /// <summary>
        /// GET method LogOut a user and remove identity
        /// </summary>
        /// <returns>Redirect to home</returns>
        [Route("LogOut")]
        public ActionResult LogOut()
        {
            try
            {
                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignOut("ApplicationCookie");
                return RedirectToAction("index", "home");
            }
            catch (Exception exp)
            {
                return RedirectToAction("index", "home");
            }
        }
    }
}
