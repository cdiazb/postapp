﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Posts.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private PostBLL _post = new PostBLL();

        /// <summary>
        /// Get method to display al approved posts
        /// </summary>
        /// <returns>Redirect to home index to display post data</returns>
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = _post.GetApprovedPosts();
            return View(posts);
        }
    }
}