﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Business;
using Model;

namespace Posts.Controllers
{
    public class CommentsController : Controller
    {
        private CommentsBLL _comment = new CommentsBLL();

        /// <summary>
        /// GET method to validate Comments by Post
        /// </summary>
        /// <param name="idPost"> int value Post Id</param>
        /// <returns>Redirect to Comment Index to display all comments</returns>
        public ActionResult Index(int? idPost)
        {
            if (idPost == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var comments = _comment.GetCommentsByPost((int)idPost);
            ViewBag.idPost = idPost;
            return View(comments.ToList());
        }

        /// <summary>
        /// POST method to create a new comment
        /// </summary>
        /// <param name="comment"> comment object with comment1 data and fkpost</param>
        /// <returns>Redirect to comment index</returns>
        [HttpPost]
        public ActionResult Create([Bind(Include = "Comment1,FkPost")] Comment comment)
        {
            if(!string.IsNullOrEmpty(comment.Comment1))
                _comment.CreateComment(comment.Comment1, comment.FkPost);
            
            return RedirectToAction("index", new { idPost = comment.FkPost });
        }
        
    }
}
