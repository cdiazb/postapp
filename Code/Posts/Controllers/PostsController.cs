﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Business;
using Model;

namespace Posts.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private PostBLL _post = new PostBLL();

        /// <summary>
        /// GET method to display post by writer
        /// </summary>
        /// <returns>Redirect to post Index to display writer posts</returns>
        [Authorize(Roles = "1")]
        public ActionResult Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int IdUser = Convert.ToInt32(identity.Claims.Where(p => p.Type.Contains("primarysid")).FirstOrDefault().Value.ToString());
            var posts = _post.GetPostsByWriter(IdUser);
            return View(posts.ToList());
        }

        /// <summary>
        /// GET method to display post Approved by Editor
        /// </summary>
        /// <returns>Redirect to post index to display approved editor posts</returns>
        [Authorize(Roles = "2")]
        public ActionResult Approved()
        {
            var identity = (ClaimsIdentity)User.Identity;
            int IdUser = Convert.ToInt32(identity.Claims.Where(p => p.Type.Contains("primarysid")).FirstOrDefault().Value.ToString());
            var posts = _post.GetPostsByIdEditor(IdUser);
            return View("~/Views/Posts/index.cshtml", posts.ToList());
        }

        /// <summary>
        /// GET method to display Pending Posts
        /// </summary>
        /// <returns>Redirect to post Index to display Pending Posts</returns>
        [Authorize(Roles = "2")]
        public ActionResult Pending()
        {
            var posts = _post.GetPendingPosts(); 

            return View("~/Views/Posts/index.cshtml", posts);
        }

        /// <summary>
        /// Get method to display post details
        /// </summary>
        /// <param name="id"> int value post Id</param>
        /// <returns>Redirect to post detail</returns>
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var post = _post.GetPostsById((int)id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        /// <summary>
        /// Get method to display create post
        /// </summary>
        /// <returns>Redirect to create post</returns>
        [HttpGet]
        [Authorize(Roles = "1")]
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// POST method to Create a new post
        /// </summary>
        /// <param name="post"> post Object</param>
        /// <returns>Redirect to post index</returns>
        /// 
        [HttpPost]
        [Authorize(Roles = "1")]
        public ActionResult Create([Bind(Include = "Id,Title,Text")] Post post)
        {
            if (ModelState.IsValid)
            {
                var identity = (ClaimsIdentity)User.Identity;
                int IdUser = Convert.ToInt32(identity.Claims.Where(p => p.Type.Contains("primarysid")).FirstOrDefault().Value.ToString());
                _post.CreatePost(post.Title, post.Text, IdUser);
                return RedirectToAction("Index");
            }

            return View(post);
        }

        /// <summary>
        /// Get method to Edit post
        /// </summary>
        /// <returns>Redirect to edit post</returns>
        [Authorize(Roles = "1")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var post = _post.GetPostsById((int)id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        /// <summary>
        /// POST method to Edit a post
        /// </summary>
        /// <param name="post"> post Object</param>
        /// <returns>Redirect to post index</returns>
        [HttpPost]
        [Authorize(Roles = "1")]
        public ActionResult Edit([Bind(Include = "Id,Title,Text")] Post post)
        {
            if (ModelState.IsValid)
            {
                _post.EditPost(post.Id, post.Title, post.Text);
                return RedirectToAction("Index");
            }
            return View(post);
        }
        /// <summary>
        /// Get method to Submit a post
        /// </summary>
        /// <param name="id"> int value post id</param>
        /// <returns>Redirect to post index</returns>
        [Authorize(Roles = "1")]
        public ActionResult SubmitPost(int id)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                int IdUser = Convert.ToInt32(identity.Claims.Where(p => p.Type.Contains("primarysid")).FirstOrDefault().Value.ToString());
                bool valid = _post.SubmitPost(id, IdUser);
                if(valid)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Details", "Posts", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("Details", "Posts", new { id = id});
            }
        }
        /// <summary>
        /// Get method to Approve a post
        /// </summary>
        /// <param name="id"> int value post id</param>
        /// <returns>Redirect to post index</returns>
        [Authorize(Roles = "2")]
        public ActionResult ApprovePost(int id)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                int IdUser = Convert.ToInt32(identity.Claims.Where(p => p.Type.Contains("primarysid")).FirstOrDefault().Value.ToString());
                bool valid = _post.ApprovePost(id, IdUser);
                if (valid)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Details", "Posts", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("Details", "Posts", new { id = id });
            }
        }

        /// <summary>
        /// Get method to Reject a post
        /// </summary>
        /// <param name="id"> int value post id</param>
        /// <returns>Redirect to post index</returns>
        [Authorize(Roles = "2")]
        public ActionResult RejectPost(int id)
        {
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                int IdUser = Convert.ToInt32(identity.Claims.Where(p => p.Type.Contains("primarysid")).FirstOrDefault().Value.ToString());
                bool valid = _post.RejectPost(id, IdUser);
                if (valid)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Details", "Posts", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("Details", "Posts", new { id = id });
            }
        }

        /// <summary>
        /// Get method to Delete a post
        /// </summary>
        /// <param name="id"> int value post id</param>
        /// <returns>Redirect to post index</returns>
        [Authorize(Roles = "2")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                bool valid = _post.DeletePost(id);
                if (valid)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Details", "Posts", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("Details", "Posts", new { id = id });
            }
        }

    }
}
