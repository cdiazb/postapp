﻿using Business;
using Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace Posts.Controllers
{
    public class PostApiController : ApiController
    {
        private PostBLL _post = new PostBLL();

        /// <summary>
        /// Get method to select all pending posts
        /// </summary>
        /// <returns>The result with posts List or error message</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetPendingPost")]
        public IHttpActionResult GetPendingPost()
        {
            try
            {
                List<GeneralPost> posts = _post.GetPendingPosts();
                return new NegotiatedContentResult<List<GeneralPost>>(HttpStatusCode.OK, posts, this);
            }
            catch (Exception ex)
            {
                return new NegotiatedContentResult<string>(HttpStatusCode.InternalServerError, ex.Message, this);
            }
        }

        /// <summary>
        /// POST method to reject a post
        /// </summary>
        /// <param name="post"> post to be rejected</param>
        /// <returns>The result with the message or error</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("RejectPostApi")]
        public IHttpActionResult RejectPostApi([FromBody]Post post)
        {
            try
            {
                bool valid = _post.RejectPost(post.Id, (int)post.FkEditor);
                if(valid)
                    return new NegotiatedContentResult<string>(HttpStatusCode.OK, "Post Rejected", this);
                else
                    return new NegotiatedContentResult<string>(HttpStatusCode.BadRequest, "The information is not right", this);
            }
            catch (Exception ex)
            {
                return new NegotiatedContentResult<string>(HttpStatusCode.InternalServerError, ex.Message, this);
            }
        }

        /// <summary>
        /// POST method to Approve a post
        /// </summary>
        /// <param name="post"> post to be approved</param>
        /// <returns>The result with the message or error</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("ApprovePostApi")]
        public IHttpActionResult ApprovePostApi([FromBody]Post post)
        {
            try
            {
                bool valid = _post.ApprovePost(post.Id, (int)post.FkEditor);
                if (valid)
                    return new NegotiatedContentResult<string>(HttpStatusCode.OK, "Post Approved", this);
                else
                    return new NegotiatedContentResult<string>(HttpStatusCode.BadRequest, "The information is not right", this);
            }
            catch (Exception ex)
            {
                return new NegotiatedContentResult<string>(HttpStatusCode.InternalServerError, ex.Message, this);
            }
        }
    }
}
