namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.Spatial;
    using System.Linq;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Comments = new HashSet<Comment>();
            Posts = new HashSet<Post>();
            Posts1 = new HashSet<Post>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        public int FkRol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Post> Posts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Post> Posts1 { get; set; }

        public virtual Role Role { get; set; }


        /// <summary>
        /// Method get all users
        /// </summary>
        /// <returns>A list of User Object</returns>
        public List<User> GetUsers()
        {
            List<User> users = new List<User>();
            try
            {
                using (var ctx = new DataContext())
                {
                    users = ctx.Users.ToList();
                }
            }
            catch (Exception exp)
            {

            }
            return users;
        }


        /// <summary>
        /// Method User By Id
        /// </summary>
        /// <param name="id"> int value User Id</param>
        /// <returns>A User Object</returns>
        public User GetUserById(int id)
        {
            User user = new User();
            try
            {
                using (var ctx = new DataContext())
                {
                    user = ctx.Users.Where(x => x.Id == id).SingleOrDefault();
                }
            }
            catch (Exception)
            {

            }
            return user;
        }

        /// <summary>
        /// Method get all comments by post
        /// </summary>
        /// <param name="_userName"> string value UserName</param>
        /// <param name="_password"> string value encrypted password</param>
        /// <returns>A User Object</returns>
        public User GetUserByUserNameAndPassword(string _userName, string _password)
        {
            User user = null;
            try
            {
                using (var ctx = new DataContext())
                {
                    user = ctx.Users.Where(x => x.UserName == _userName && x.Password == _password).SingleOrDefault();
                }
            }
            catch (Exception exp)
            {

            }
            return user;
        }


    }
}
