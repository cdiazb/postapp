namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity;
    using System.Linq;

    public partial class Comment
    {
        public int Id { get; set; }

        [Column("Comment")]
        [Required]
        [StringLength(500)]
        public string Comment1 { get; set; }

        public DateTime CreationDate { get; set; }

        public int FkPost { get; set; }

        public int? FkUser { get; set; }

        public virtual Post Post { get; set; }

        public virtual User User { get; set; }


        /// <summary>
        /// Method get all comments
        /// </summary>
        /// <returns>A list of Comment Objects </returns>
        public List<Comment> GetComments()
        {
            List<Comment> comments = new List<Comment>();
            try
            {
                using (var ctx = new DataContext())
                {
                    comments = ctx.Comments.ToList();
                }
            }
            catch (Exception exp)
            {

            }
            return comments;
        }

        /// <summary>
        /// Method get a comment by id
        /// </summary>
        /// <param name="id"> int value Comment Id</param>
        /// <returns>A Comment Objects</returns>
        public Comment GetCommentById(int id)
        {
            Comment comment = new Comment();
            try
            {
                using (var ctx = new DataContext())
                {
                    comment = ctx.Comments.Where(x => x.Id == id).SingleOrDefault();
                }
            }
            catch (Exception)
            {

            }
            return comment;
        }

        /// <summary>
        /// Method get all comments by post
        /// </summary>
        /// <param name="idPost"> int value Post Id</param>
        /// <returns>A list of Comment Objects that were written for this posts</returns>
        public List<Comment> GetCommentsByPost(int idPost)
        {
            List<Comment> comments = new List<Comment>();
            try
            {
                using (var ctx = new DataContext())
                {
                    comments = ctx.Comments.Where(x => x.FkPost == idPost).ToList();
                }
            }
            catch (Exception)
            {

            }
            return comments;
        }


        /// <summary>
        /// Method to store or update a comment
        /// </summary>
        /// <returns>Bool value to show if it was create or updatede</returns>
        public bool SaveComment()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    if (this.Id > 0)
                    {
                        
                        ctx.Entry(this).State = EntityState.Modified;
                    }
                    else
                    {
                        this.CreationDate = DateTime.Now;
                        ctx.Entry(this).State = EntityState.Added;
                    }
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Method to delete a comment
        /// </summary>
        /// <returns>Bool value to show if it was deleted</returns>
        public bool DeleteComment()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    ctx.Entry(this).State = EntityState.Deleted;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }
    }
}
