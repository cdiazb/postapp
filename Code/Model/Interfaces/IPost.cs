﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Interfaces
{
    public interface IPost
    {
        List<GeneralPost> GetPosts();
        GeneralPost GetPostById(int id);

        Post GetSimplePostById(int id);

        List<GeneralPost> GetPostByWriter(int idWriter);

        List<GeneralPost> GetPostByEditor(int idEditor);

        List<GeneralPost> GetPostByStatus(int idStatus);

        bool SavePost();

        bool DeletePost();

        bool SubmitPost();

        bool PublishPost();

        bool RejectPost();


    }
}
