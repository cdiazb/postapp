﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class GeneralPost 
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? SubmitDate { get; set; }

        public DateTime? ApproveDate { get; set; }

        public int FkStatus { get; set; }

        public int FkWriter { get; set; }

        public int? FkEditor { get; set; }
        public string WriterName { get; set; }
        public string EditorName { get; set; }
        public string StatusName { get; set; }

    }
}
