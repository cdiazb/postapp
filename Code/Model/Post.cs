namespace Model
{
    using Model.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.Spatial;
    using System.Linq;

    public partial class Post: IPost
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Post()
        {
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? SubmitDate { get; set; }

        public DateTime? ApproveDate { get; set; }

        public int FkStatus { get; set; }

        public int FkWriter { get; set; }

        public int? FkEditor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual User User { get; set; }

        public virtual Status Status { get; set; }

        public virtual User User1 { get; set; }


        /// <summary>
        /// Method get all posts
        /// </summary>
        /// <returns>A List of Posts Objects</returns>
        public List<GeneralPost> GetPosts()
        {
            List<GeneralPost> posts = new List<GeneralPost>();
            try
            {
                using (var ctx = new DataContext())
                {
                    posts = ctx.Posts
                        .Select(y => new GeneralPost()
                        {
                            Id = y.Id,
                            Title = y.Title,
                            Text = y.Text,
                            CreationDate = y.CreationDate,
                            SubmitDate = y.SubmitDate,
                            ApproveDate = y.ApproveDate,
                            FkStatus = y.FkStatus,
                            FkWriter = y.FkWriter,
                            FkEditor = y.FkEditor,
                            WriterName = y.User1.Name,
                            EditorName = y.User.Name,
                            StatusName = y.Status.Status1
                        }).ToList();
                }
            }
            catch (Exception exp)
            {

            }
            return posts;
        }

        /// <summary>
        /// Method get a post by id
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <returns>A GeneralPost Object</returns>
        public GeneralPost GetPostById(int id)
        {
            GeneralPost post = new GeneralPost();
            try
            {
                using (var ctx = new DataContext())
                {
                    post = ctx.Posts.Where(x => x.Id == id)
                        .Select(y => new GeneralPost()
                        {
                            Id = y.Id,
                            Title = y.Title,
                            Text = y.Text,
                            CreationDate = y.CreationDate,
                            SubmitDate = y.SubmitDate,
                            ApproveDate = y.ApproveDate,
                            FkStatus = y.FkStatus,
                            FkWriter = y.FkWriter,
                            FkEditor = y.FkEditor,
                            WriterName = y.User1.Name,
                            EditorName = y.User.Name,
                            StatusName = y.Status.Status1
                        })
                        .SingleOrDefault();
                }
            }
            catch (Exception)
            {

            }
            return post;
        }

        /// <summary>
        /// Method get a post by id
        /// </summary>
        /// <param name="id"> int value Post Id</param>
        /// <returns>A Post Object</returns>
        public Post GetSimplePostById(int id)
        {
            Post post = new Post();
            try
            {
                using (var ctx = new DataContext())
                {
                    post = ctx.Posts.Where(x => x.Id == id).SingleOrDefault();
                }
            }
            catch (Exception)
            {

            }
            return post;
        }

        /// <summary>
        /// Method get all post by writer Id
        /// </summary>
        /// <param name="idWriter"> int value writer Id</param>
        /// <returns>A list of GeneralPost Objects</returns>
        public List<GeneralPost> GetPostByWriter(int idWriter)
        {
            List<GeneralPost> post = new List<GeneralPost>();
            try
            {
                using (var ctx = new DataContext())
                {
                    post = ctx.Posts.Where(x => x.FkWriter == idWriter)
                        .Select(y => new GeneralPost()
                        {
                            Id = y.Id,
                            Title = y.Title,
                            Text = y.Text,
                            CreationDate = y.CreationDate,
                            SubmitDate = y.SubmitDate,
                            ApproveDate = y.ApproveDate,
                            FkStatus = y.FkStatus,
                            FkWriter = y.FkWriter,
                            FkEditor = y.FkEditor,
                            WriterName = y.User1.Name,
                            EditorName = y.User.Name,
                            StatusName = y.Status.Status1
                        })
                        .ToList();
                }
            }
            catch (Exception)
            {

            }
            return post;
        }

        /// <summary>
        /// Method get all post by Editor Id
        /// </summary>
        /// <param name="idEditor"> int value editor Id</param>
        /// <returns>A list of GeneralPost Objects</returns>
        public List<GeneralPost> GetPostByEditor(int idEditor)
        {
            List<GeneralPost> post = new List<GeneralPost>();
            try
            {
                using (var ctx = new DataContext())
                {
                    post = ctx.Posts.Where(x => x.FkEditor == idEditor)
                        .Select(y => new GeneralPost()
                        {
                            Id = y.Id,
                            Title = y.Title,
                            Text = y.Text,
                            CreationDate = y.CreationDate,
                            SubmitDate = y.SubmitDate,
                            ApproveDate = y.ApproveDate,
                            FkStatus = y.FkStatus,
                            FkWriter = y.FkWriter,
                            FkEditor = y.FkEditor,
                            WriterName = y.User1.Name,
                            EditorName = y.User.Name,
                            StatusName = y.Status.Status1
                        })
                        .ToList();
                }
            }
            catch (Exception)
            {

            }
            return post;
        }

        /// <summary>
        /// Method get all post by Status Id
        /// </summary>
        /// <param name="idStatus"> int value Status Id</param>
        /// <returns>A list of GeneralPost Objects</returns>
        public List<GeneralPost> GetPostByStatus(int idStatus)
        {
            List<GeneralPost> Posts = new List<GeneralPost>();
            try
            {
                using (var ctx = new DataContext())
                {
                    Posts = ctx.Posts.Where(p => p.FkStatus == idStatus)
                        .Select(y => new GeneralPost()
                        {
                            Id = y.Id,
                            Title = y.Title,
                            Text = y.Text,
                            CreationDate = y.CreationDate,
                            SubmitDate = y.SubmitDate,
                            ApproveDate = y.ApproveDate,
                            FkStatus = y.FkStatus,
                            FkWriter = y.FkWriter,
                            FkEditor = y.FkEditor,
                            WriterName = y.User1.Name,
                            EditorName = y.User.Name,
                            StatusName = y.Status.Status1
                        })
                        .ToList();

                }
            }
            catch (Exception exp)
            {

            }
            return Posts;
        }

        /// <summary>
        /// Method to create or update a post
        /// </summary>
        /// <returns>Bool value to show if the post was created or updated</returns>
        public bool SavePost()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    this.FkStatus = 1;
                    if (this.Id > 0)
                    {
                        this.FkEditor = null;
                        ctx.Entry(this).State = EntityState.Modified;
                    }
                    else
                    {
                        ctx.Entry(this).State = EntityState.Added;
                    }
                    ctx.SaveChanges();
                }
            }
            catch (Exception exp)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Method to delete a post
        /// </summary>
        /// <returns>Bool value to show if the post was deleted</returns>
        public bool DeletePost()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    ctx.Entry(this).State = EntityState.Deleted;
                    ctx.SaveChanges();
                }
            }
            catch (Exception exp)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Method to submit a post
        /// </summary>
        /// <returns>Bool value to show if the post was sumbited</returns>
        public bool SubmitPost()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    this.FkStatus = 2;
                    this.SubmitDate = DateTime.Now;
                    ctx.Entry(this).State = EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Method to Approve a post
        /// </summary>
        /// <returns>Bool value to show if the post was Approved</returns>
        public bool PublishPost()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    this.FkStatus = 3;
                    this.ApproveDate = DateTime.Now;
                    ctx.Entry(this).State = EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Method to reject a post
        /// </summary>
        /// <returns>Bool value to show if the post was rejected</returns>
        public bool RejectPost()
        {
            bool status = true;
            try
            {
                using (var ctx = new DataContext())
                {
                    this.FkStatus = 4;
                    ctx.Entry(this).State = EntityState.Modified;
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }


    }

}


