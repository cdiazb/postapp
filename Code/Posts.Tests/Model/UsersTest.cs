﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace Posts.Tests.Model
{
    [TestClass]
    public class UsersTest
    {

        static User _userW = new User()
        {
            Name = "testW",
            UserName = "testW",
            Password = "testW",
            FkRol = 1
        };
        static User _userE = new User()
        {
            Name = "testE",
            UserName = "testE",
            Password = "testE",
            FkRol = 2
        };

        /// <summary>
        /// Set up method to initialize the tests
        /// </summary>
        /// <param name="context"></param>
        [ClassInitialize()]
        public static void SetUp(TestContext context)
        {
            
        }

        /// <summary>
        /// Clean up class to remove the data created for the tests
        /// </summary>
        [ClassCleanup()]
        public static void ClassCleanup()
        {
           
        }


        
    }
}
