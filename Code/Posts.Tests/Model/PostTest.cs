﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using Model.Interfaces;
using Moq;

namespace Posts.Tests.Model
{
    [TestClass]
    public class PostTest
    {
        public static List<GeneralPost> postlist;
        public static GeneralPost firstPost = new GeneralPost()
        {
            Id = 1,
            Title = "First Title",
            Text = "First Text",
            CreationDate = DateTime.Now,
            SubmitDate = DateTime.Now,
            ApproveDate = DateTime.Now,
            FkStatus = 3,
            FkWriter = 1,
            FkEditor = 2,
            WriterName = "writer",
            EditorName = "editor",
            StatusName = "Approved"
        };

        public static GeneralPost secondPost = new GeneralPost()
        {
            Id = 2,
            Title = "Second Title",
            Text = "Second Text",
            CreationDate = DateTime.Now,
            SubmitDate = DateTime.Now,
            ApproveDate = DateTime.Now,
            FkStatus = 3,
            FkWriter = 1,
            FkEditor = 2,
            WriterName = "writer",
            EditorName = "editor",
            StatusName = "Pending"
        };
        public static Post simplePost = new Post()
        {
            Id = 1,
            Title = "First Simple Title",
            Text = "First Simple Text",
            CreationDate = DateTime.Now,
            SubmitDate = DateTime.Now,
            ApproveDate = DateTime.Now,
            FkStatus = 3,
            FkWriter = 1,
            FkEditor = 2
        };

        /// <summary>
        /// Set up method to initialize the tests
        /// </summary>
        /// <param name="context"></param>
        [ClassInitialize()]
        public static void SetUp(TestContext context)
        {
            postlist = new List<GeneralPost>();
            postlist.Add(firstPost);
            postlist.Add(secondPost);
        }

        [TestMethod]
        public void GetPosts_success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                //mockedRepo.Setup(m => m.GetPostById(1)).Returns(post);  // Setup the response to the call using that parameter value
                mockedRepo.Setup(m => m.GetPosts()).Returns(postlist);

                //Act
                var response = mockedRepo.Object.GetPosts();
                //var responses = mockedRepo.Object.GetPostById(1);

                // Assert
                Assert.AreEqual(response.Count, 2);
            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostById_success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostById(1)).Returns(firstPost);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostById(1);
                var secondResponse = mockedRepo.Object.GetPostById(2);
                // Assert
                Assert.AreEqual(response.Title, "First Title");
                Assert.AreEqual(response.FkStatus, 3);
                Assert.IsNotNull(secondResponse);
            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostById_NoExist()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostById(1)).Returns(firstPost);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostById(3);

                // Assert
                Assert.IsNull(response);
            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetSimplePostById_Success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetSimplePostById(1)).Returns(simplePost);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetSimplePostById(1);

                // Assert
                Assert.IsNotNull(response);
                Assert.AreEqual(response.Title, "First Simple Title");

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetSimplePostById_NoExist()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetSimplePostById(1)).Returns(simplePost);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetSimplePostById(2);

                // Assert
                Assert.IsNull(response);
            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostByWriter_Success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostByWriter(1)).Returns(postlist);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostByWriter(1);

                // Assert
                Assert.IsNotNull(response);
                Assert.AreEqual(response.Count, 2);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostByWriter_NoExist()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostByWriter(1)).Returns(postlist);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostByWriter(2);

                // Assert
                Assert.IsNull(response);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostByEditor_Success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostByEditor(2)).Returns(postlist);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostByEditor(2);

                // Assert
                Assert.IsNotNull(response);
                Assert.AreEqual(response.Count, 2);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostByEditor_NoExist()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostByEditor(2)).Returns(postlist);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostByEditor(1);

                // Assert
                Assert.IsNull(response);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostByStatus_Success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostByStatus(3)).Returns(postlist);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostByStatus(3);

                // Assert
                Assert.IsNotNull(response);
                Assert.AreEqual(response.Count, 2);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void GetPostByStatus_NoExist()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.GetPostByStatus(3)).Returns(postlist);  // Setup the response to the call using that parameter value

                //Act
                var response = mockedRepo.Object.GetPostByStatus(1);

                // Assert
                Assert.IsNull(response);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void SavePost_Success()
        {
            try
            {
                //Assume
                var mockedRepo = new Mock<IPost>();  // Creata a mock of the interface
                mockedRepo.Setup(m => m.SavePost()).Returns(true);  // Setup the response to the call using that parameter value
                //simplePost.Id = 0;
                //Act
                var response = simplePost.SavePost();

                // Assert
                Assert.IsNotNull(response);
                //Assert.AreEqual(response.Count, 2);

            }
            catch (Exception exp)
            {
                Assert.IsTrue(false);
            }
        }


    }
}
