﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Posts.Controllers;
using System.Web.Mvc;

namespace Posts.Tests.Controllers
{
    /// <summary>
    /// Summary description for UsersControllerTest
    /// </summary>
    [TestClass]
    public class UsersControllerTest
    {
        [TestMethod]
        public void ValidateUser()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void LogOut()
        {
            HomeController controller = new HomeController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
