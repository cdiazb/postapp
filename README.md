# Zemoga AppPost
Project that allows writers to create posts and submit them to be published also allows editors to approve or reject those posts.


# Data
	You can find two folders, each one has the data that I wil explain:
		# Code: you will find the complete source code for the MVC project(Posts) where also you can find the API Code, also you can see a SQL file (BackUpBD_Posts.sql) with the structure for the DB and the basic information that has to be in the DB (Roles, Status)

		# Publishable: You will find the Publishable for the mvc project (Web), also you can see a bak file (BackUpBD_Posts.bak) to restore this on your database, this file allready has the basic information



# Deploy
	1) Download the repository from https://gitlab.com/cdiazb/postapp.git 
	2) Create a new db as PostsDb on sqlserver 2012 or higher with windows authentication
	3) Restore the BackUpBD_Posts.bak file in the db that you create on step 2, this file is in /Publishable/BackUpBD_Posts.bak
	4) Check that the tables roles and status have information, otherwise delete the db an do steps 2 and 3
	5) Move the Publishable for web and api to the folder that you are going to publish it(could be the same folder that is now) the folder you have to move is "/Publishable" 
	6) Open the IIS Administrator (You should have already configurate your IIS)
	7) RSight click on sites and selet add web site
	8) Write the name to your website, select the folder where you put the Publishable, click on connect as and put the user's info that have permissons on the server, finally select the ip and port on which it will run.
	9) After thar you go to the publishable folder and open de web.config file, in the tag <connectionStrings> be sure that you change the source for your server name or database instance and if you create the database with other name change it in catalog, also you have to replace the id and password for the one you used to create the DB.
	10) now you can go to the browser and put the ip and port to see that is now working

	11) In the db you can find 2 user to see how it works:
		user: writer, pass: writer
		user: editor, pass: editor

	





# How to work
	#	Api: The routes to use the API are:
        1) (GET) /GetPendingPost     => Get Pending posts
        2) (POST) /RejectPostApi     => Reject Post, the body you have to send a JSON "{"Id" : 7, "FkEditor": 2}"
        3) (POST) /ApprovePostApi     => Approve Post, the body you have to send a JSON "{"Id" : 7, "FkEditor": 2}" 

# Time
    #The that was used to create this project was 10 hours. Includin DB creation, API and WEB


# Contact Information
	Name: CARLOS HERNAN DIAZ BROCHERO
	Phone: 3188278807
	Email: CARLOSH.86.DIAZ@GMAIL.COM